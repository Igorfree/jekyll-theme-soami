# SoamI theme

This is a custom Jekyll theme for my personal website, all written by hand in Emacs. If you speak German you will get the pun of "SoamI" by slightly rearranging my full name. The website is self-hosted on a Raspberry Pi - thus the main logo, indicating what hardware you are talking to, when you open the website. You can use the pixel-art as per the Free Art License (a copy is in the assets/img folder).

## Try it out
Prerequisites: you must have bundler and Jekyll installed. Visit [Jekyll website][jekyll] for detailed instructions

This theme is very easy to try out and see what it's all about!

1. clone this repostiry
2. run `bundle install` in the newly created directory
3. run `bundle exec jekyll serve`
4. visit `localhost:4000` in your browser

## Installation
1. clone this repository
2. create a new [Jekyll][jekyll] website
3. set the theme of your new website in _config.yml: `theme: jekyll-theme-soami`
4. open the Gemfile and load this theme like this:
```
gem 'jekyll-theme-soami',
    :git => '/home/[username]/[path]/jekyll-theme-soami',
    :branch => '[branch-name]'
```
make sure you have checked out the branch you are specifying here!
5. run `bundle install` in your new website's root directory
6. now your website should start with this theme

## Making changes
If you change something locally and wish your site to load them, it might be necessary to do the following:
1. create a local commit of your changes to this theme
2. remove the Gemfile.lock of your website (NOT this project)
3. run `bundle install` in your website's root directory

This will force Jekyll to get the newest version of the theme, instead of loading the old one before the change.
When doing the above, make sure you are creating the commit on the same branch, that is specified inside the Gemfile of your website!

## Post licensing

This theme shows a license for each post. I am using the Creative Commons Attribution Noderivatives 4.0 for blog posts and no license for all other categories (like photos). Which puts the posts under the authors copyright[^1]. Please pay attention when using the post templates and set your preffered license, otherwise the footer below each post may contain a link to the Creative Commons Attribution Noderivatives 4.0.

[^1] in Germany, AFAIK. Don't consider this binding legal advice, as I am no lawyer

[jekyll]: https://jekyllrb.com
