---
layout: post
title: "Erste Fotos"
date: 2024-04-18 12:34:06 +0200
categories: photos
lang: de
tag: de
---

Erste Urlaubsfotos

`![Mein Traumurlaub](/photos/bild.jpg)`

Die Bilder können direkt unter `/photos` abgelegt werden und sind mit dem o.g. Markdown in den Posts leicht zu referenzieren
