---
layout: post
title: "Erster Post"
date: 2024-04-18 12:25:05 +0200
categories: blog
lang: de
tag: de
description: "Erster Post. Template für Posts"
license:
  short: CC BY-ND 4.0 Deed
  link: https://creativecommons.org/licenses/by-nd/4.0/deed.de
---

## Template für Posts

- layout muss korrekt gesetzt sein, in diesem Fall "post" (damit der Post nicht als roher Text gerendert wird, sondern sich dem Layout der Webseite anpasst). Vorbelegung durch _config.yml in der defaults-Sektion möglich
- title muss korrekt gesetzt sein (am besten in " "), sonst hat der Post kein Titel und somit keine Überschrift
- date muss das Datum der Erstellung tragen
- category muss gesetzt sein (in diesem Fall auf "blog"). Das ist wichtig, denn dadurch werden mehrere Sachen gesteuert:
  - Kategoriebasierte Anzeige der Posts (Filtern von Blog, Portfolio, etc.)
  - Leiste fuer schnelle Navigation in der Mitte oben (wenn category nicht gesetzt ist, wird die Leiste home > [post.title] Anzeigen, anstatt home > blog > [post.title])
- lang muss gesetzt sein (in diesem Fall auf de), damit die Sprachumschaltung mit Polyglot-Plugin funktioniert
- tag muss gesetzt sein (in diesem Fall auf de), damit die Pagination der Posts nicht alle Sprachen zusammen anzeigt (das Paginate-v2 Plugin versteht den "lang"-Tag nicht, den Polyglot nutzt)
- description sollte gesetzt werden, als Mittel für SEO. 
- license sollte gesetzt werden, wenn der Inhalt des Posts unter einer Lizenz steht, welche vom standardmässigem Kopierschutzrecht abweicht

## Erstellen von Posts

Weitere Informationen zur Erstellung von [Posts bei Jekyll](https://jekyllrb.com/docs/posts/ "Posts bei Jekyll")

