---
layout: post
title: "First post"
date: 2024-04-18 12:25:05 +0200
categories: blog
lang: en
tag: en
description: "First post. Template for posts"
license: CC
---

## Template for posts

- layout must be set correctly, in this case "post" (otherwise the post will be rendered as raw text). It is possible to set a default in the defaults section of _config.yml
- title must be set correctly (ideally within " "), otherwise the post won't have a title, ergo no heading
- date must contain the date of post creation
- category must be set (in this case "blog"). This is important, because multiple things react to this:
  - category-based listing of posts (filtering of blog-posts, portfolio-posts, etc.)
  - quick-navigation bar won't work correctly (if category is not set it will show home > [post.title], instead of home > blog > [post.title])
- lang must be set (en in this case), for the language switcher of polyglot-plugin to function correctly
- tag must be set correctly (in this case en), so that pagination doesn't show all the languages at once (paginate-v2-plugin does not understand the "lang" tag which polyglot is using)
- description should be set, as a means for SEO
- license should be set, if the contents are licensed under something else than the default copyright


## Creating Posts

More information about creating your [posts in jekyll](https://jekyllrb.com/docs/posts/ "posts in jekyll")

