---
layout: post
title: "First draft"
category: blog
lang: en
tag: en
description: "First draft. Demo of draft mechanism and different markdown options"
license: CC
---

This is the first draft in english. It demos different markdown conversion to html

A First Level Header
====================

A Second Level Header
---------------------

Now is the time for all good men to come to
the aid of their country. This is just a
regular paragraph.

The quick brown fox jumped over the lazy
dog's back.

### Header 3

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> ## This is an H2 in a blockquote

