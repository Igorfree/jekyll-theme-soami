Gem::Specification.new do |spec|
  spec.name          = "jekyll-theme-soami"
  spec.version       = "0.1"
  spec.authors       = ["Igor Zobin"]
  spec.email         = ["i.zobin@proton.me"]

  spec.summary       = "Highly accessible, responsive, and minimalistic dark-theme for Jekyll."
  spec.homepage      = "https://gitlab.com/Igorfree/jekyll-theme-soami"
  spec.license       = "GPLv3"

#  require 'rake'
#  spec.files = FileList['_includes/*',
#                        '_layouts/*',
#                        '_sass/*',
#                        '_data/*',
#                        'assets/**/*',
#                        '[A-Z]*'].to_a
  spec.files         = `git ls-files -z`.split("\x0").select do |f|
    f.match(%r{^((_data|_includes|_layouts|_sass|assets)/|(LICENSE|README|CHANGELOG)((\.(txt|md|markdown)|$)))}i)
  end  
#  spec.files         = `git ls-files -z`.split("\x0").select { |f|
#    f.match(%r!^((_(includes|layouts|sass|data))|assets\/|README|LICENSE)!i)
#  }

  spec.metadata = {
    "bug_tracker_uri"   => "https://gitlab.com/Igorfree/jekyll-theme-soami/-/issues",
    "documentation_uri" => "https://gitlab.com/Igorfree/jekyll-theme-soami/-/blob/main/README.md",
    "homepage_uri"      => "https://zobin.eu",
    "source_code_uri"   => "https://gitlab.com/Igorfree/jekyll-theme-soami",
    "wiki_uri"          => "https://gitlab.com/Igorfree/jekyll-theme-soami/-/blob/main/README.md",
    "plugin_type"       => "theme"
  }

  spec.required_ruby_version = ">= 3.0"

  spec.add_runtime_dependency "jekyll", "~> 4.3"
  spec.add_runtime_dependency "jekyll-paginate-v2" 
  spec.add_runtime_dependency "jekyll-polyglot"
  spec.add_runtime_dependency "jekyll-sitemap"
  spec.add_runtime_dependency "jekyll-seo-tag"

end

